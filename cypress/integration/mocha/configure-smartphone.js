/// <reference types="cypress" />

context('User Journey - PDP', () => {
    describe('The user should be able to configure smartphone and tariff and put both into the cart', () => {
        it('Go to smartphone page', () => {
            cy.clearLocalStorage()
            cy.clearCookies()

            cy.visit('/smartphones/apple/iphone-11')

            cy.contains('Apple iPhone 11')
                .should('exist');
        })

        it('Browse through images', () => {
            cy.get('.container .article-gallery .thumbnails .slick-slide')
                .eq(0)
                .click()
            cy.get('.container .article-gallery')
                .first()
                .scrollIntoView()
            cy.wait(2000)

            cy.get('.container .article-gallery .thumbnails .slick-slide')
                .eq(1)
                .click()
            cy.get('.container .article-gallery')
                .first()
                .scrollIntoView()
            cy.wait(2000)

            cy.get('.container .article-gallery .thumbnails .slick-slide')
                .eq(2)
                .click()
            cy.get('.container .article-gallery')
                .first()
                .scrollIntoView()
            cy.wait(2000)
        })

        it('Select memory capacity', () => {
            cy.get('[data-acceptance-test="product-details-memory-variation"]')
                .eq(2)
                .click()

            cy.wait(2000)
        })

        it('Select color', () => {
            cy.get('[data-acceptance-test="product-details-color-variation"]')
                .eq(2)
                .click()

            cy.wait(2000)
        })

        it('Select tariff', () => {
            cy.contains('Tarif auswählen')
                .first()
                .click()

            cy.wait(2000)
        })

        it('Add to cart', () => {
            cy.contains('Jetzt bestellen')
                .first()
                .click()

            cy.wait(2000)
        })

        it('Verify cart', () => {
            cy.get('.cart-details--device')
                .should('exist')

            cy.get('.cart-details--tariff')
                .should('exist')
        })
    })
})
