Then('there should be a smartphone in the cart', () => {
    cy.get('.cart-details--device')
        .should('exist')
})

Then('there should be a tariff in the cart', () => {
    cy.get('.cart-details--tariff')
        .should('exist')
})
