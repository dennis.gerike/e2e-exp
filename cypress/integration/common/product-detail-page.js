When('the user goes to the smartphone detail page', () => {
    cy.clearLocalStorage()
    cy.clearCookies()

    cy.visit('/smartphones/apple/iphone-11')

    cy.contains('Zustimmen und weiter')
        .click()
})

When('the user browses through the image gallery', () => {
    cy.get('.container .article-gallery .thumbnails .slick-slide')
        .eq(0)
        .click()
    cy.get('.container .article-gallery')
        .first()
        .scrollIntoView()
    cy.wait(2000)

    cy.get('.container .article-gallery .thumbnails .slick-slide')
        .eq(1)
        .click()
    cy.get('.container .article-gallery')
        .first()
        .scrollIntoView()
    cy.wait(2000)

    cy.get('.container .article-gallery .thumbnails .slick-slide')
        .eq(2)
        .click()
    cy.get('.container .article-gallery')
        .first()
        .scrollIntoView()
    cy.wait(2000)
});

When('the user configures the memory capacity of the smartphone', () => {
    cy.get('[data-acceptance-test="product-details-memory-variation"]')
        .eq(2)
        .click()

    cy.wait(2000)
})

When('the user selects a color for the smartphone', () => {
    cy.get('[data-acceptance-test="product-details-color-variation"]')
        .eq(2)
        .click()

    cy.wait(2000)
})

When('the user selects a tariff', () => {
    cy.contains('Tarif auswählen')
        .first()
        .click()

    cy.wait(2000)
})

When('the user adds the current selection to the cart', () => {
    cy.contains('Jetzt bestellen')
        .first()
        .click()

    cy.wait(2000)
})
