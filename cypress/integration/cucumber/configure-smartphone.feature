Feature: User Journey - PDP

  Scenario: The user should be able to configure smartphone and tariff and put both into the cart
    When the user goes to the smartphone detail page
    And the user browses through the image gallery
    And the user configures the memory capacity of the smartphone
    And the user selects a color for the smartphone
    And the user selects a tariff
    And the user adds the current selection to the cart
    Then there should be a smartphone in the cart
    And there should be a tariff in the cart